package br.com.felipe.boaviagem;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	private EditText user;
	private EditText password;
	private String errorLogin;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		initViews();
	}

	public void initViews() {
		user = (EditText) findViewById(R.id.usuario);
		password = (EditText) findViewById(R.id.senha);
		errorLogin = getResources().getString(R.string.login_error);
	}

	public void login(View v) {
		if (user.getText().toString().equals("aluno")
				&& password.getText().toString().equals("123")) {
			startActivity(new Intent(this, DashBoardActivity.class));
			finish();
		} else {
			Toast.makeText(this, errorLogin, Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
